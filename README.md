# Form Validation - React App (Platform Electives Objective)
A simple React App that showcases the ability of react in Form Input Validation. In the application, there are three input fields that are needed to be satisifed in order for the submit buttom to be clickable, namely: Firstname, Lastname, and Email. Each of these fields have individual validation wherein these are used in the main validation for disabling and enabling the submit button. The main concept that was elaborated in this application was the practice of form validation.

## Setup

```
npm install
npm run start
```
